package sys.circuitbreaker;

import org.junit.Before;
import org.junit.Test;
import sys.circuitbreaker.test.builder.EventBroadcasterBuilder;
import sys.circuitbreaker.test.builder.OperationBuilder;

import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static sys.circuitbreaker.test.builder.OperationBuilder.executeWithFailure;
import static sys.circuitbreaker.test.builder.OperationBuilder.executeWithResult;
import static sys.circuitbreaker.test.util.ThreadUtils.sleep;

public class TestCircuitBreakerIntegration {

    private static final int THRESHOLD = 5;
    private static final long OPEN_STATE_DURATION_MILLIS = 500;

    private CircuitBreaker circuitBreaker;
    private OperationBuilder operationBuilder;

    @Before
    public void setUp() {
        circuitBreaker = new CircuitBreakerImpl(
                new EventBroadcasterBuilder().build(),
                new CircuitBuilder()
                        .eventBroadcaster(new EventBroadcasterBuilder().build())
                        .openStateTimer(new TimeoutTimerImpl(new EventBroadcasterBuilder().build()))
                        .failureCounter(new CounterImpl(THRESHOLD, 5))
                        .successCounter(new CounterImpl(THRESHOLD, 5))
                        .options(new CircuitOptions.Builder()
                                .state(State.CLOSED)
                                .openStateDurationMillis(OPEN_STATE_DURATION_MILLIS)
                                .build())
                        .build(),
                Executors.newFixedThreadPool(8));

        operationBuilder = new OperationBuilder();
    }

    @Test
    public void circuitIsOpen_subsequentOperationsAreNotExecuted() throws Throwable {
        runFailedOperationsUntilCircuitIsOpen();

        // The following should not get executed because the circuit is now in OPEN state.
        for (int i = 0; i < 3; i++) {
            Operation<Void, Throwable> op = operationBuilder.key("KEY").build();
            executeWithFailure(op, new Exception());

            circuitBreaker.submit(op);

            sleep(50);

            verify(op, never()).execute();
        }
    }

    @Test
    public void circuitIsHalfOpen_subsequentOperationsAreExecuted() throws Throwable {
        runFailedOperationsUntilCircuitIsOpen();

        Operation<Void, Throwable> op1 = operationBuilder.key("KEY_1").build();
        executeWithFailure(op1, new Exception());

        // This operation will not be executed because the circuit is now in OPEN state.
        circuitBreaker.submit(op1);

        sleep(50);

        verify(op1, never()).execute();

        // By this time, the circuit should now be in the HALF_OPEN state.
        sleep(OPEN_STATE_DURATION_MILLIS);

        Operation<Void, Throwable> op2 = operationBuilder.key("KEY_2").build();
        executeWithResult(op2, null);

        Operation<Void, Throwable> op3 = operationBuilder.key("KEY_3").build();
        executeWithResult(op3, null);

        // These operations will be executed because the circuit is now in HALF_OPEN state.
        circuitBreaker.submit(op2);
        circuitBreaker.submit(op3);

        sleep(50);

        verify(op2).execute();
        verify(op3).execute();

        assertEquals(State.HALF_OPEN, circuitBreaker.getCircuitState());
    }

    @Test
    public void circuitIsHalfOpen___subsequentOperationsAreExecuted_oneOperationFailed___circuitIsOpen() throws Throwable {
        runFailedOperationsUntilCircuitIsOpen();

        // By this time, the circuit should now be in the HALF_OPEN state.
        sleep(OPEN_STATE_DURATION_MILLIS);

        runSuccessOperationsNTimes(2);
        runFailedOperationsNTimes(1);

        Operation<Void, Throwable> op1 = operationBuilder.key("KEY_1").build();
        executeWithResult(op1, null);

        Operation<Void, Throwable> op2 = operationBuilder.key("KEY_2").build();
        executeWithResult(op2, null);

        // These operations will not be executed because the circuit is now in OPEN state.
        circuitBreaker.submit(op1);
        circuitBreaker.submit(op2);

        sleep(50);

        verify(op1, never()).execute();
        verify(op2, never()).execute();

        assertEquals(State.OPEN, circuitBreaker.getCircuitState());
    }

    @Test
    public void circuitIsHalfOpen___subsequentOperationsAreExecuted_successThresholdIsReached___circuitIsClosed() throws Throwable {
        runFailedOperationsUntilCircuitIsOpen();

        // By this time, the circuit should now be in the HALF_OPEN state.
        sleep(OPEN_STATE_DURATION_MILLIS);

        runSuccessOperationsUntilCircuitIsClosed();

        Operation<Void, Throwable> op1 = operationBuilder.key("KEY_1").build();
        executeWithResult(op1, null);

        Operation<Void, Throwable> op2 = operationBuilder.key("KEY_2").build();
        executeWithResult(op2, null);

        // These operations will be executed because the circuit is now in CLOSED state.
        circuitBreaker.submit(op1);
        circuitBreaker.submit(op2);

        sleep(50);

        verify(op1).execute();
        verify(op2).execute();

        assertEquals(State.CLOSED, circuitBreaker.getCircuitState());
    }

    /**
     * Breaking the circuit by running failed operations until the failure threshold is reached.
     */
    private void runFailedOperationsUntilCircuitIsOpen() {
        runFailedOperationsNTimes(THRESHOLD);
    }

    /**
     * Running failed operations N times.
     */
    private void runFailedOperationsNTimes(int n) {
        for (int i = 0; i < n; i++) {
            Operation<Void, Throwable> op = operationBuilder.key("KEY").build();
            executeWithFailure(op, new Exception());

            circuitBreaker.submit(op);
        }
    }

    /**
     * Restoring the circuit by running successful operations until the success threshold is reached.
     */
    private void runSuccessOperationsUntilCircuitIsClosed() {
        runSuccessOperationsNTimes(THRESHOLD);
    }

    /**
     * Running successful operations N times.
     */
    private void runSuccessOperationsNTimes(int n) {
        for (int i = 0; i < n; i++) {
            Operation<Void, Throwable> op = operationBuilder.key("KEY").build();
            executeWithResult(op, new Exception());

            circuitBreaker.submit(op);
        }
    }

}
