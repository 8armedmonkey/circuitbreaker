package sys.circuitbreaker;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCircuitOptionsBuilder {

    private CircuitOptions.Builder builder;

    @Before
    public void setUp() {
        builder = new CircuitOptions.Builder();
    }

    @Test
    public void state_open_circuitOptionsStateIsSetToInitialState() {
        builder.state(State.OPEN);
        assertEquals(State.OPEN, builder.build().getState());
    }

    @Test
    public void state_halfOpen_circuitOptionsStateIsSetToInitialState() {
        builder.state(State.HALF_OPEN);
        assertEquals(State.HALF_OPEN, builder.build().getState());
    }

    @Test
    public void state_closed_circuitOptionsStateIsSetToInitialState() {
        builder.state(State.CLOSED);
        assertEquals(State.CLOSED, builder.build().getState());
    }

    @Test
    public void openStateDurationMillis_circuitOptionsDurationMillisIsSet() {
        builder.openStateDurationMillis(5000);
        assertEquals(5000, builder.build().getOpenStateDurationMillis());
    }

}
