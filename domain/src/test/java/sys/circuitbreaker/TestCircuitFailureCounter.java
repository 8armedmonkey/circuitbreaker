package sys.circuitbreaker;

import org.junit.Before;
import org.junit.Test;
import sys.circuitbreaker.test.builder.EventBroadcasterBuilder;

import static org.junit.Assert.assertEquals;

public class TestCircuitFailureCounter {

    private static final int THRESHOLD = 5;

    private Circuit circuit;
    private CircuitBuilder circuitBuilder;
    private CircuitOptions.Builder circuitOptionsBuilder;
    private Counter failureCounter;

    @Before
    public void setUp() {
        failureCounter = new CounterImpl(THRESHOLD, 0);

        circuitBuilder = new CircuitBuilder()
                .eventBroadcaster(new EventBroadcasterBuilder().build())
                .openStateTimer(new TimeoutTimerImpl(new EventBroadcasterBuilder().build()))
                .successCounter(new CounterImpl(THRESHOLD, 0))
                .failureCounter(failureCounter);

        // Important to set the open state duration, otherwise the tests will be flaky due to race condition.
        circuitOptionsBuilder = new CircuitOptions.Builder()
                .openStateDurationMillis(500);
    }

    @Test
    public void onOperationFailed_stateIsOpen_ignored() {
        circuit = circuitBuilder
                .options(circuitOptionsBuilder.state(State.OPEN).build())
                .build();

        circuit.onOperationFailed();

        assertEquals(0, failureCounter.current());
    }

    @Test
    public void onOperationFailed_stateIsClosed_failureCounterIsIncremented() {
        circuit = circuitBuilder
                .options(circuitOptionsBuilder.state(State.CLOSED).build())
                .build();

        circuit.onOperationFailed();
        circuit.onOperationFailed();

        assertEquals(2, failureCounter.current());
    }

    @Test
    public void onOperationFailed___stateIsClosed_failureCounterThresholdIsReached___stateChangedToOpen() {
        circuit = circuitBuilder
                .options(circuitOptionsBuilder.state(State.CLOSED).build())
                .build();

        for (int i = 0; i < THRESHOLD; i++) {
            circuit.onOperationFailed();
        }

        assertEquals(State.OPEN, circuit.getState());
    }

    @Test
    public void onOperationFailed_stateIsHalfOpen_stateChangedToOpen() {
        circuit = circuitBuilder
                .options(circuitOptionsBuilder.state(State.HALF_OPEN).build())
                .build();

        circuit.onOperationFailed();

        assertEquals(State.OPEN, circuit.getState());
    }

}
