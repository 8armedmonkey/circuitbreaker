package sys.circuitbreaker;

import org.junit.Before;
import org.junit.Test;
import sys.circuitbreaker.test.builder.EventBroadcasterBuilder;
import sys.circuitbreaker.test.builder.OperationBuilder;

import java.util.concurrent.Executors;

import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static sys.circuitbreaker.test.util.ThreadUtils.sleep;

public class TestCircuitBreaker {

    private static final int THRESHOLD = 5;

    private CircuitBreaker circuitBreaker;
    private OperationSuccessHandler operationSuccessHandler;
    private OperationFailedHandler operationFailedHandler;
    private CircuitBuilder circuitBuilder;
    private CircuitOptions.Builder circuitOptionsBuilder;

    @Before
    public void setUp() {
        operationSuccessHandler = mock(OperationSuccessHandler.class);
        operationFailedHandler = mock(OperationFailedHandler.class);

        circuitBuilder = new CircuitBuilder()
                .eventBroadcaster(new EventBroadcasterBuilder().build())
                .openStateTimer(new TimeoutTimerImpl(new EventBroadcasterBuilder().build()))
                .failureCounter(new CounterImpl(THRESHOLD, 0))
                .successCounter(new CounterImpl(THRESHOLD, 0));

        circuitOptionsBuilder = new CircuitOptions.Builder()
                .openStateDurationMillis(500);
    }

    @Test
    public void submit_circuitStateIsClosed_operationIsExecuted() throws Throwable {
        circuitBreaker = new CircuitBreakerImpl(
                new EventBroadcasterBuilder().build(),
                circuitBuilder
                        .options(circuitOptionsBuilder.state(State.CLOSED).build())
                        .build(),
                Executors.newFixedThreadPool(8));

        circuitBreaker.subscribe(OperationSuccess.class, operationSuccessHandler);
        circuitBreaker.subscribe(OperationFailed.class, operationFailedHandler);

        Operation<Void, Throwable> op = new OperationBuilder().key("KEY").build();
        OperationBuilder.executeWithResult(op, null);

        circuitBreaker.submit(op);

        sleep(50);

        verify(op).execute();
    }

    @Test
    public void submit_circuitStateIsOpen_operationFailedImmediately() throws Throwable {
        circuitBreaker = new CircuitBreakerImpl(
                new EventBroadcasterBuilder().build(),
                circuitBuilder
                        .options(circuitOptionsBuilder.state(State.OPEN).build())
                        .build(),
                Executors.newFixedThreadPool(8));

        circuitBreaker.subscribe(OperationSuccess.class, operationSuccessHandler);
        circuitBreaker.subscribe(OperationFailed.class, operationFailedHandler);

        Operation<Void, Throwable> op = new OperationBuilder().key("KEY").build();
        OperationBuilder.executeWithResult(op, null);

        circuitBreaker.submit(op);

        sleep(50);

        verify(op, never()).execute();
    }

    @Test
    public void submit_circuitStateIsHalfOpen_operationIsExecuted() throws Throwable {
        circuitBreaker = new CircuitBreakerImpl(
                new EventBroadcasterBuilder().build(),
                circuitBuilder
                        .options(circuitOptionsBuilder.state(State.HALF_OPEN).build())
                        .build(),
                Executors.newFixedThreadPool(8));

        circuitBreaker.subscribe(OperationSuccess.class, operationSuccessHandler);
        circuitBreaker.subscribe(OperationFailed.class, operationFailedHandler);

        Operation<Void, Throwable> op = new OperationBuilder().key("KEY").build();
        OperationBuilder.executeWithResult(op, null);

        circuitBreaker.submit(op);

        sleep(50);

        verify(op).execute();
    }

    @Test
    public void clear_allOperationsAreRemoved() {
        circuitBreaker = new CircuitBreakerImpl(
                new EventBroadcasterBuilder().build(),
                circuitBuilder
                        .options(circuitOptionsBuilder.state(State.CLOSED).build())
                        .build(),
                Executors.newFixedThreadPool(8));

        Operation<Void, Throwable> op1 = new OperationBuilder().key("KEY_1").build();
        OperationBuilder.executeWithResult(op1, null);

        Operation<Void, Throwable> op2 = new OperationBuilder().key("KEY_2").build();
        OperationBuilder.executeWithResult(op2, null);

        Operation<Void, Throwable> op3 = new OperationBuilder().key("KEY_3").build();
        OperationBuilder.executeWithResult(op3, null);

        circuitBreaker.submit(op1);
        circuitBreaker.submit(op2);
        circuitBreaker.submit(op3);
        circuitBreaker.clear();

        sleep(50);

        assertFalse(circuitBreaker.contains("KEY_1"));
        assertFalse(circuitBreaker.contains("KEY_2"));
        assertFalse(circuitBreaker.contains("KEY_3"));
    }

    @Test
    public void onOperationSuccess_operationSuccessBroadcast() {
        circuitBreaker = new CircuitBreakerImpl(
                new EventBroadcasterBuilder().build(),
                circuitBuilder
                        .options(circuitOptionsBuilder.state(State.CLOSED).build())
                        .build(),
                Executors.newFixedThreadPool(8));

        circuitBreaker.subscribe(OperationSuccess.class, operationSuccessHandler);
        circuitBreaker.subscribe(OperationFailed.class, operationFailedHandler);

        Operation<Void, Throwable> op = new OperationBuilder().key("KEY").build();
        OperationBuilder.executeWithResult(op, null);

        circuitBreaker.submit(op);

        sleep(50);

        verify(operationSuccessHandler).handle(any(OperationSuccess.class));
    }

    @Test
    public void onOperationFailed_operationFailedBroadcast() {
        circuitBreaker = new CircuitBreakerImpl(
                new EventBroadcasterBuilder().build(),
                circuitBuilder
                        .options(circuitOptionsBuilder.state(State.CLOSED).build())
                        .build(),
                Executors.newFixedThreadPool(8));

        circuitBreaker.subscribe(OperationSuccess.class, operationSuccessHandler);
        circuitBreaker.subscribe(OperationFailed.class, operationFailedHandler);

        Operation<Void, Throwable> op = new OperationBuilder().key("KEY").build();
        OperationBuilder.executeWithFailure(op, new Exception());

        circuitBreaker.submit(op);

        sleep(50);

        verify(operationFailedHandler).handle(any(OperationFailed.class));
    }

}
