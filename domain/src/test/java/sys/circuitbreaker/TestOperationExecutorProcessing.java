package sys.circuitbreaker;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import sys.circuitbreaker.test.builder.EventBroadcasterBuilder;
import sys.circuitbreaker.test.builder.OperationBuilder;

import java.util.concurrent.Executors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static sys.circuitbreaker.test.util.ThreadUtils.sleep;

public class TestOperationExecutorProcessing {

    private static final int THRESHOLD = 5;

    private OperationExecutor operationExecutor;
    private OperationSuccessHandler successHandler;
    private OperationFailedHandler failedHandler;

    @Before
    public void setUp() {
        Circuit circuit = new CircuitBuilder()
                .eventBroadcaster(new EventBroadcasterBuilder().build())
                .openStateTimer(new TimeoutTimerImpl(new EventBroadcasterBuilder().build()))
                .failureCounter(new CounterImpl(THRESHOLD, 0))
                .successCounter(new CounterImpl(THRESHOLD, 0))
                .options(new CircuitOptions.Builder()
                        .state(State.CLOSED)
                        .openStateDurationMillis(500)
                        .build())
                .build();

        operationExecutor = new CircuitBreakerImpl(
                new EventBroadcasterBuilder().build(),
                circuit,
                Executors.newFixedThreadPool(8));

        operationExecutor.subscribe(OperationSuccess.class, successHandler = mock(OperationSuccessHandler.class));
        operationExecutor.subscribe(OperationFailed.class, failedHandler = mock(OperationFailedHandler.class));
    }

    @Test
    public void submit_operation_operationIsExecuted() throws Throwable {
        Operation<Void, Throwable> op = new OperationBuilder().key("KEY").build();

        operationExecutor.submit(op);
        sleep(50);

        verify(op).execute();
    }

    @Test
    public void successOperation_containsOperationReturnFalse() {
        Operation<Void, Throwable> op = new OperationBuilder().key("KEY").build();
        OperationBuilder.executeWithResult(op, null);

        operationExecutor.submit(op);
        sleep(50);

        assertFalse(operationExecutor.contains("KEY"));
    }

    @Test
    public void successOperation_operationSuccessEventBroadcast() {
        Object result = new Object();

        Operation<Void, Throwable> op = new OperationBuilder().key("KEY").build();
        OperationBuilder.executeWithResult(op, result);

        operationExecutor.submit(op);
        sleep(50);

        ArgumentCaptor<OperationSuccess> captor = ArgumentCaptor.forClass(OperationSuccess.class);
        verify(successHandler).handle(captor.capture());

        OperationSuccess operationSuccess = captor.getValue();

        assertEquals("KEY", operationSuccess.getKey());
        assertEquals(result, operationSuccess.getResult());
    }

    @Test
    public void failedOperation_containsOperationReturnFalse() {
        Operation<Void, Throwable> op = new OperationBuilder().key("KEY").build();
        OperationBuilder.executeWithFailure(op, new Exception());

        operationExecutor.submit(op);
        sleep(50);

        assertFalse(operationExecutor.contains("KEY"));
    }

    @Test
    public void failedOperation_operationFailedEventBroadcast() {
        Throwable throwable = new Exception();

        Operation<Void, Throwable> op = new OperationBuilder().key("KEY").build();
        OperationBuilder.executeWithFailure(op, throwable);

        operationExecutor.submit(op);
        sleep(50);

        ArgumentCaptor<OperationFailed> captor = ArgumentCaptor.forClass(OperationFailed.class);
        verify(failedHandler).handle(captor.capture());

        OperationFailed operationFailed = captor.getValue();

        assertEquals("KEY", operationFailed.getKey());
        assertEquals(throwable, operationFailed.getThrowable());
    }

}
