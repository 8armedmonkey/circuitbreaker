package sys.circuitbreaker;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestCounter {

    private static final int THRESHOLD = 5;
    private Counter counter;

    @Before
    public void setUp() {
        counter = new CounterImpl(THRESHOLD, 0);
    }

    @Test
    public void constructor_defaultValue() {
        assertEquals(0, new CounterImpl(THRESHOLD).current());
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_counterGtThreshold() {
        new CounterImpl(THRESHOLD, THRESHOLD + 1);
    }

    @Test
    public void increment_countIncremented() {
        counter.increment();
        assertEquals(1, counter.current());
    }

    @Test
    public void increment_thresholdReached_countNotIncrementedAnymore() {
        for (int i = 0; i <= THRESHOLD; i++) {
            counter.increment();
        }
        assertEquals(THRESHOLD, counter.current());
    }

    @Test
    public void hasReachedThreshold_counterLtThreshold() {
        assertFalse(counter.hasReachedThreshold());
    }

    @Test
    public void hasReachedThreshold_counterEqThreshold() {
        for (int i = 0; i < THRESHOLD; i++) {
            counter.increment();
        }
        assertTrue(counter.hasReachedThreshold());
    }

}
