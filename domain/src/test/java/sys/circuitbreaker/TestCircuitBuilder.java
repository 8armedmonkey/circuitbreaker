package sys.circuitbreaker;

import org.junit.Before;
import org.junit.Test;
import sys.circuitbreaker.test.builder.EventBroadcasterBuilder;
import sys.event.EventBroadcaster;

import static org.junit.Assert.assertEquals;

public class TestCircuitBuilder {

    private CircuitBuilder builder;

    @Before
    public void setUp() {
        builder = new CircuitBuilder();
    }

    @Test
    public void eventBroadcaster_eventBroadcasterIsSet() {
        EventBroadcaster eventBroadcaster = new EventBroadcasterBuilder().build();
        builder.eventBroadcaster(eventBroadcaster);

        assertEquals(eventBroadcaster, builder.eventBroadcaster);
    }

    @Test
    public void openStateTimer_openStateTimerIsSet() {
        TimeoutTimer openStateTimer = new TimeoutTimerImpl(new EventBroadcasterBuilder().build());
        builder.openStateTimer(openStateTimer);

        assertEquals(openStateTimer, builder.openStateTimer);
    }

    @Test
    public void failureCounter_failureCounterIsSet() {
        Counter failureCounter = new CounterImpl(5);
        builder.failureCounter(failureCounter);

        assertEquals(failureCounter, builder.failureCounter);
    }

    @Test
    public void successCounter_failureCounterIsSet() {
        Counter successCounter = new CounterImpl(5);
        builder.successCounter(successCounter);

        assertEquals(successCounter, builder.successCounter);
    }

    @Test
    public void options_optionsAreSet() {
        CircuitOptions options = new CircuitOptions.Builder().build();
        builder.options(options);

        assertEquals(options, builder.options);
    }

}
