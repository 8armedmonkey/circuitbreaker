package sys.circuitbreaker;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import sys.circuitbreaker.test.builder.EventBroadcasterBuilder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TestCircuitStateChange {

    private CircuitBuilder circuitBuilder;
    private CircuitOptions.Builder circuitOptionsBuilder;
    private CircuitStateChangeHandler handler;

    @Before
    public void setUp() {
        circuitBuilder = new CircuitBuilder()
                .eventBroadcaster(new EventBroadcasterBuilder().build())
                .openStateTimer(new TimeoutTimerImpl(new EventBroadcasterBuilder().build()))
                .failureCounter(new CounterImpl(5))
                .successCounter(new CounterImpl(5));

        // Important to set the open state duration, otherwise the test will be flaky due to race condition.
        circuitOptionsBuilder = new CircuitOptions.Builder()
                .openStateDurationMillis(500);

        handler = mock(CircuitStateChangeHandler.class);
    }

    @Test
    public void stateChanged_closedToOpen_circuitStateChangeBroadcast() {
        Circuit circuit = circuitBuilder
                .options(circuitOptionsBuilder.state(State.CLOSED).build())
                .build();

        circuit.subscribe(CircuitStateChange.class, handler);
        circuit.open();

        ArgumentCaptor<CircuitStateChange> captor = ArgumentCaptor.forClass(CircuitStateChange.class);
        verify(handler).handle(captor.capture());

        assertEquals(State.CLOSED, captor.getValue().getOldState());
        assertEquals(State.OPEN, captor.getValue().getNewState());
    }

    @Test
    public void stateChanged_openToHalfOpen_circuitStateChangeBroadcast() {
        Circuit circuit = circuitBuilder
                .options(circuitOptionsBuilder.state(State.OPEN).build())
                .build();

        circuit.subscribe(CircuitStateChange.class, handler);
        circuit.halfOpen();

        ArgumentCaptor<CircuitStateChange> captor = ArgumentCaptor.forClass(CircuitStateChange.class);
        verify(handler).handle(captor.capture());

        assertEquals(State.OPEN, captor.getValue().getOldState());
        assertEquals(State.HALF_OPEN, captor.getValue().getNewState());
    }

    @Test
    public void stateChanged_halfOpenToClosed_circuitStateChangeBroadcast() {
        Circuit circuit = circuitBuilder
                .options(circuitOptionsBuilder.state(State.HALF_OPEN).build())
                .build();

        circuit.subscribe(CircuitStateChange.class, handler);
        circuit.close();

        ArgumentCaptor<CircuitStateChange> captor = ArgumentCaptor.forClass(CircuitStateChange.class);
        verify(handler).handle(captor.capture());

        assertEquals(State.HALF_OPEN, captor.getValue().getOldState());
        assertEquals(State.CLOSED, captor.getValue().getNewState());
    }

    @Test
    public void stateChanged_halfOpenToOpen_circuitStateChangeBroadcast() {
        Circuit circuit = circuitBuilder
                .options(circuitOptionsBuilder.state(State.HALF_OPEN).build())
                .build();

        circuit.subscribe(CircuitStateChange.class, handler);
        circuit.open();

        ArgumentCaptor<CircuitStateChange> captor = ArgumentCaptor.forClass(CircuitStateChange.class);
        verify(handler).handle(captor.capture());

        assertEquals(State.HALF_OPEN, captor.getValue().getOldState());
        assertEquals(State.OPEN, captor.getValue().getNewState());
    }

}
