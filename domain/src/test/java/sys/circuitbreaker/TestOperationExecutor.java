package sys.circuitbreaker;

import org.junit.Before;
import org.junit.Test;
import sys.circuitbreaker.test.builder.EventBroadcasterBuilder;
import sys.circuitbreaker.test.builder.OperationBuilder;

import java.util.concurrent.Executors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestOperationExecutor {

    private static final int THRESHOLD = 5;

    private OperationExecutor operationExecutor;
    private OperationBuilder builder;

    @Before
    public void setUp() {
        Circuit circuit = new CircuitBuilder()
                .eventBroadcaster(new EventBroadcasterBuilder().build())
                .openStateTimer(new TimeoutTimerImpl(new EventBroadcasterBuilder().build()))
                .failureCounter(new CounterImpl(THRESHOLD, 0))
                .successCounter(new CounterImpl(THRESHOLD, 0))
                .options(new CircuitOptions.Builder()
                        .state(State.CLOSED)
                        .openStateDurationMillis(500)
                        .build())
                .build();

        operationExecutor = new CircuitBreakerImpl(
                new EventBroadcasterBuilder().build(),
                circuit,
                Executors.newFixedThreadPool(8));

        builder = new OperationBuilder();
    }

    @Test
    public void submit_operation_operationIsScheduledToBeExecuted() {
        operationExecutor.submit(builder.key("KEY").build());
        assertTrue(operationExecutor.contains("KEY"));
    }

    @Test
    public void remove_key_operationIsCancelled() {
        operationExecutor.submit(builder.key("KEY").build());
        operationExecutor.remove("KEY");
        assertFalse(operationExecutor.contains("KEY"));
    }

    @Test
    public void clear_allOperationsAreCancelled() {
        operationExecutor.submit(builder.key("KEY_1").build());
        operationExecutor.submit(builder.key("KEY_2").build());
        operationExecutor.submit(builder.key("KEY_3").build());
        operationExecutor.submit(builder.key("KEY_4").build());

        operationExecutor.clear();

        assertFalse(operationExecutor.contains("KEY_1"));
        assertFalse(operationExecutor.contains("KEY_2"));
        assertFalse(operationExecutor.contains("KEY_3"));
        assertFalse(operationExecutor.contains("KEY_4"));
    }

}
