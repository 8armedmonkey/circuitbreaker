package sys.circuitbreaker;

import org.junit.Before;
import org.junit.Test;
import sys.circuitbreaker.test.builder.EventBroadcasterBuilder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static sys.circuitbreaker.test.util.ThreadUtils.sleep;

public class TestCircuit {

    private static final int FAILURE_THRESHOLD = 5;
    private static final int SUCCESS_THRESHOLD = 5;

    private TimeoutTimer timer;
    private Counter failureCounter;
    private Counter successCounter;
    private Circuit circuit;

    @Before
    public void setUp() {
        timer = spy(new TimeoutTimerImpl(new EventBroadcasterBuilder().build()));
        failureCounter = spy(new CounterImpl(FAILURE_THRESHOLD));
        successCounter = spy(new CounterImpl(SUCCESS_THRESHOLD));

        circuit = new CircuitImpl(
                new EventBroadcasterBuilder().build(),
                timer,
                failureCounter,
                successCounter,
                new CircuitOptions.Builder()
                        .state(State.CLOSED)
                        .openStateDurationMillis(500)
                        .build());
    }

    @Test
    public void open_timeoutTimerStarted() {
        circuit.open();
        verify(timer).start(anyLong());
    }

    @Test
    public void open_stateIsOpen() {
        circuit.open();
        assertEquals(State.OPEN, circuit.getState());
    }

    @Test
    public void timerTimeout_stateIsHalfOpen() {
        circuit.open();
        sleep(600);
        assertEquals(State.HALF_OPEN, circuit.getState());
    }

    @Test
    public void close_failureCounterIsReset() {
        circuit.close();
        verify(failureCounter).reset();
    }

    @Test
    public void close_stateIsClosed() {
        circuit.close();
        assertEquals(State.CLOSED, circuit.getState());
    }

    @Test
    public void halfOpen_successCounterIsReset() {
        circuit.halfOpen();
        verify(successCounter).reset();
    }

    @Test
    public void halfOpen_stateIsHalfOpen() {
        circuit.halfOpen();
        assertEquals(State.HALF_OPEN, circuit.getState());
    }

}
