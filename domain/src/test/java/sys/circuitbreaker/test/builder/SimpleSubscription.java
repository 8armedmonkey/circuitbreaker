package sys.circuitbreaker.test.builder;

import sys.event.Event;
import sys.event.EventBroadcaster;
import sys.event.EventHandler;

import java.util.Set;

class SimpleSubscription implements EventBroadcaster.Subscription {

    private final Set<EventHandler<Event>> set;
    private final EventHandler<Event> handler;

    SimpleSubscription(Set<EventHandler<Event>> set, EventHandler<Event> handler) {
        this.set = set;
        this.handler = handler;
    }

    @Override
    public void unsubscribe() {
        set.remove(handler);
    }

}
