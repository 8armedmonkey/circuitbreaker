package sys.circuitbreaker.test.builder;

import sys.event.EventBroadcaster;

public class EventBroadcasterBuilder {

    public EventBroadcaster build() {
        return new SimpleEventBroadcaster();
    }

}
