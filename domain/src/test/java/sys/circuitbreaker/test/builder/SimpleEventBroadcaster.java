package sys.circuitbreaker.test.builder;

import sys.event.Event;
import sys.event.EventBroadcaster;
import sys.event.EventHandler;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

class SimpleEventBroadcaster implements EventBroadcaster {

    private Map<String, Set<EventHandler<Event>>> eventHandlers;

    SimpleEventBroadcaster() {
        eventHandlers = new HashMap<>();
    }

    @Override
    public void broadcast(Event event) {
        Set<EventHandler<Event>> set = eventHandlers.get(event.getClass().getName());

        if (set != null) {
            for (EventHandler<Event> eventHandler : set) {
                eventHandler.handle(event);
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public <E extends Event> Subscription subscribe(Class<E> eventClass, EventHandler<E> handler) {
        if (!eventHandlers.containsKey(eventClass.getName())) {
            eventHandlers.put(eventClass.getName(), new HashSet<>());
        }

        Set<EventHandler<Event>> set = eventHandlers.get(eventClass.getName());
        set.add((EventHandler<Event>) handler);

        return new SimpleSubscription(set, (EventHandler<Event>) handler);
    }

}
