package sys.circuitbreaker.test.builder;

import sys.circuitbreaker.Operation;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OperationBuilder {

    private String key;

    public static void executeWithResult(Operation op, Object result) {
        try {
            when(op.execute()).thenReturn(result);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static void executeWithFailure(Operation op, Throwable failure) {
        try {
            when(op.execute()).thenThrow(failure);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public OperationBuilder key(String key) {
        this.key = key;
        return this;
    }

    @SuppressWarnings("unchecked")
    public <Result, Failure extends Throwable> Operation<Result, Failure> build() {
        Operation<Result, Failure> op = mock(Operation.class);
        when(op.key()).thenReturn(key);

        return op;
    }

}
