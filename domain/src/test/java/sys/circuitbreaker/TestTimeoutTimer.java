package sys.circuitbreaker;

import org.junit.Before;
import org.junit.Test;
import sys.circuitbreaker.test.builder.EventBroadcasterBuilder;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static sys.circuitbreaker.test.util.ThreadUtils.sleep;

public class TestTimeoutTimer {

    private TimeoutTimer timer;

    @Before
    public void setUp() {
        timer = new TimeoutTimerImpl(new EventBroadcasterBuilder().build());
    }

    @Test
    public void start_timerIsStarted() {
        timer.start(10000);
        assertTrue(timer.isRunning());
    }

    @Test
    public void stop_timerIsStopped() {
        timer.start(10000);
        timer.stop();
        assertFalse(timer.isRunning());
    }

    @Test
    public void start_multipleTimes_updatedWithLatestDurationMillis() {
        TimerTimeoutHandler handler = mock(TimerTimeoutHandler.class);
        timer.subscribe(TimerTimeout.class, handler);

        timer.start(10000);
        timer.start(1000);
        sleep(1100);

        verify(handler).handle(any(TimerTimeout.class));
        assertFalse(timer.isRunning());
    }

    @Test
    public void timeout_timerTimeoutEventBroadcast() {
        TimerTimeoutHandler handler = mock(TimerTimeoutHandler.class);
        timer.subscribe(TimerTimeout.class, handler);

        timer.start(1000);
        sleep(1100);

        verify(handler).handle(any(TimerTimeout.class));
        assertFalse(timer.isRunning());
    }

    @Test
    public void notTimeoutYet_noTimerTimeoutEventBroadcast() {
        TimerTimeoutHandler handler = mock(TimerTimeoutHandler.class);
        timer.subscribe(TimerTimeout.class, handler);

        timer.start(1000);
        sleep(500);

        verify(handler, never()).handle(any(TimerTimeout.class));
        assertTrue(timer.isRunning());
    }

}
