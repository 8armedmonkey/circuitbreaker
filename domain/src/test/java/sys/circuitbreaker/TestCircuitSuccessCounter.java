package sys.circuitbreaker;

import org.junit.Before;
import org.junit.Test;
import sys.circuitbreaker.test.builder.EventBroadcasterBuilder;

import static org.junit.Assert.assertEquals;

public class TestCircuitSuccessCounter {

    private static final int THRESHOLD = 5;

    private Circuit circuit;
    private CircuitBuilder circuitBuilder;
    private CircuitOptions.Builder circuitOptionsBuilder;
    private Counter successCounter;

    @Before
    public void setUp() {
        successCounter = new CounterImpl(THRESHOLD, 0);

        circuitBuilder = new CircuitBuilder()
                .openStateTimer(new TimeoutTimerImpl(new EventBroadcasterBuilder().build()))
                .successCounter(successCounter)
                .failureCounter(new CounterImpl(THRESHOLD, 0))
                .eventBroadcaster(new EventBroadcasterBuilder().build());

        // Important to set the open state duration, otherwise the tests will be flaky due to race condition.
        circuitOptionsBuilder = new CircuitOptions.Builder()
                .openStateDurationMillis(500);
    }

    @Test
    public void onOperationSuccess_stateIsClosed_ignored() {
        circuit = circuitBuilder
                .options(circuitOptionsBuilder.state(State.CLOSED).build())
                .build();

        circuit.onOperationSuccess();

        assertEquals(0, successCounter.current());
    }

    @Test
    public void onOperationSuccess_stateIsHalfOpen_successCounterIsIncremented() {
        circuit = circuitBuilder
                .options(circuitOptionsBuilder.state(State.HALF_OPEN).build())
                .build();

        circuit.onOperationSuccess();
        circuit.onOperationSuccess();

        assertEquals(2, successCounter.current());
    }

    @Test
    public void onOperationSuccess___stateIsHalfOpen_successCounterThresholdReached___stateChangedToClosed() {
        circuit = circuitBuilder
                .options(circuitOptionsBuilder.state(State.HALF_OPEN).build())
                .build();

        for (int i = 0; i < THRESHOLD; i++) {
            circuit.onOperationSuccess();
        }

        assertEquals(State.CLOSED, circuit.getState());
    }

}
