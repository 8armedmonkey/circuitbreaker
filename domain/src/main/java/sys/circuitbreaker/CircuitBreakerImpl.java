package sys.circuitbreaker;

import sys.event.Event;
import sys.event.EventBroadcaster;
import sys.event.EventHandler;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

class CircuitBreakerImpl implements CircuitBreaker {

    private final EventBroadcaster eventBroadcaster;
    private final ExecutorService executorService;
    private final Map<String, Future> futures;
    private final Circuit circuit;

    CircuitBreakerImpl(
            EventBroadcaster eventBroadcaster,
            Circuit circuit,
            ExecutorService executorService) {

        this.eventBroadcaster = eventBroadcaster;
        this.circuit = circuit;
        this.executorService = executorService;
        this.futures = Collections.synchronizedMap(new HashMap<>());
    }

    @Override
    public State getCircuitState() {
        return circuit.getState();
    }

    @Override
    public <Result, Failure extends Throwable> void submit(Operation<Result, Failure> operation) {
        if (getCircuitState() != State.OPEN) {
            futures.put(operation.key(), executorService.submit(() -> execute(operation)));
        } else {
            eventBroadcaster.broadcast(new OperationFailed(operation.key(), new OpenCircuitException()));
        }
    }

    @Override
    public void remove(String key) {
        Future f;

        if ((f = futures.remove(key)) != null) {
            f.cancel(false);
        }
    }

    @Override
    public void clear() {
        String[] keys = futures.keySet().toArray(new String[0]);

        for (String key : keys) {
            remove(key);
        }
    }

    @Override
    public boolean contains(String key) {
        return futures.containsKey(key);
    }

    @Override
    public <E extends Event> EventBroadcaster.Subscription subscribe(Class<E> eventClass, EventHandler<E> eventHandler) {
        return eventBroadcaster.subscribe(eventClass, eventHandler);
    }

    private <Result, Failure extends Throwable> void execute(Operation<Result, Failure> operation) {
        futures.remove(operation.key());

        if (getCircuitState() != State.OPEN) {
            try {
                Result result = operation.execute();
                onOperationSuccess(operation, result);

            } catch (Throwable t) {
                onOperationFailed(operation, t);
            }
        } else {
            onOperationFailed(operation, new OpenCircuitException());
        }
    }

    private <Result, Failure extends Throwable> void onOperationSuccess(
            Operation<Result, Failure> operation, Result result) {

        circuit.onOperationSuccess();
        eventBroadcaster.broadcast(new OperationSuccess<>(operation.key(), result));
    }

    private <Result, Failure extends Throwable> void onOperationFailed(
            Operation<Result, Failure> operation, Throwable throwable) {

        circuit.onOperationFailed();
        eventBroadcaster.broadcast(new OperationFailed(operation.key(), throwable));
    }

}
