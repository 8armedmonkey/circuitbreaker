package sys.circuitbreaker;

public interface Counter {

    void increment();

    void reset();

    int current();

    boolean hasReachedThreshold();

}
