package sys.circuitbreaker;

import sys.event.EventHandler;

@SuppressWarnings("WeakerAccess")
public interface OperationSuccessHandler extends EventHandler<OperationSuccess> {
}
