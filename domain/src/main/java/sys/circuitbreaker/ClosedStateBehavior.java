package sys.circuitbreaker;

class ClosedStateBehavior implements StateBehavior {

    private final Circuit circuit;
    private final Counter failureCounter;

    ClosedStateBehavior(Circuit circuit, Counter failureCounter) {
        this.circuit = circuit;
        this.failureCounter = failureCounter;
    }

    @Override
    public void onInit() {
        failureCounter.reset();
    }

    @Override
    public void onOperationSuccess() {
        // Do nothing.
    }

    @Override
    public void onOperationFailed() {
        failureCounter.increment();

        if (failureCounter.hasReachedThreshold()) {
            circuit.open();
        }
    }

}
