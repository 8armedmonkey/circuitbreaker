package sys.circuitbreaker;

import sys.event.EventHandler;

public interface TimerTimeoutHandler extends EventHandler<TimerTimeout> {
}
