package sys.circuitbreaker;

public enum State {

    CLOSED, HALF_OPEN, OPEN

}
