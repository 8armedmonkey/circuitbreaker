package sys.circuitbreaker;

import sys.event.Event;
import sys.event.EventBroadcaster;
import sys.event.EventHandler;

public interface TimeoutTimer {

    void start(long durationMillis);

    void stop();

    boolean isRunning();

    <E extends Event> EventBroadcaster.Subscription subscribe(Class<E> eventClass, EventHandler<E> eventHandler);
}
