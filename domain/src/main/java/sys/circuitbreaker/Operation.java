package sys.circuitbreaker;

public interface Operation<Result, Failure extends Throwable> {

    String key();

    Result execute() throws Failure;

}