package sys.circuitbreaker;

class OpenStateBehavior implements StateBehavior {

    private final TimeoutTimer openStateTimer;
    private final long openStateDurationMillis;

    OpenStateBehavior(TimeoutTimer openStateTimer, long openStateDurationMillis) {
        this.openStateTimer = openStateTimer;
        this.openStateDurationMillis = openStateDurationMillis;
    }

    @Override
    public void onInit() {
        openStateTimer.start(openStateDurationMillis);
    }

    @Override
    public void onOperationSuccess() {
        // Do nothing.
    }

    @Override
    public void onOperationFailed() {
        // Do nothing.
    }

}
