package sys.circuitbreaker;

import sys.event.Event;
import sys.event.EventBroadcaster;
import sys.event.EventHandler;

class CircuitImpl implements Circuit {

    private final EventBroadcaster eventBroadcaster;
    private final TimeoutTimer openStateTimer;
    private final Counter failureCounter;
    private final Counter successCounter;
    private final long openStateDurationMillis;
    private State state;
    private StateBehavior stateBehavior;

    CircuitImpl(
            EventBroadcaster eventBroadcaster,
            TimeoutTimer openStateTimer,
            Counter failureCounter,
            Counter successCounter,
            CircuitOptions options) {

        this.eventBroadcaster = eventBroadcaster;
        this.openStateTimer = openStateTimer;
        this.failureCounter = failureCounter;
        this.successCounter = successCounter;
        this.openStateDurationMillis = options.getOpenStateDurationMillis();

        setState(options.getState());

        this.openStateTimer.subscribe(TimerTimeout.class, event -> halfOpen());
    }

    @Override
    public synchronized State getState() {
        return state;
    }

    @Override
    public synchronized void close() {
        setStateAndBroadcast(State.CLOSED);
        stateBehavior.onInit();
    }

    @Override
    public synchronized void open() {
        setStateAndBroadcast(State.OPEN);
        stateBehavior.onInit();
    }

    @Override
    public synchronized void halfOpen() {
        setStateAndBroadcast(State.HALF_OPEN);
        stateBehavior.onInit();
    }

    @Override
    public synchronized void onOperationSuccess() {
        stateBehavior.onOperationSuccess();
    }

    @Override
    public synchronized void onOperationFailed() {
        stateBehavior.onOperationFailed();
    }

    @Override
    public <E extends Event> EventBroadcaster.Subscription subscribe(Class<E> eventClass, EventHandler<E> eventHandler) {
        return eventBroadcaster.subscribe(eventClass, eventHandler);
    }

    private void setStateAndBroadcast(State newState) {
        State oldState = state;

        setState(newState);
        eventBroadcaster.broadcast(new CircuitStateChange(oldState, state));
    }

    private void setState(State newState) {
        state = newState;
        updateStateBehavior();
    }

    private void updateStateBehavior() {
        switch (state) {
            case CLOSED:
                stateBehavior = new ClosedStateBehavior(this, failureCounter);
                break;

            case OPEN:
                stateBehavior = new OpenStateBehavior(openStateTimer, openStateDurationMillis);
                break;

            case HALF_OPEN:
                stateBehavior = new HalfOpenStateBehavior(this, successCounter);
                break;
        }
    }

}
