package sys.circuitbreaker;

import sys.event.Event;
import sys.event.EventBroadcaster;
import sys.event.EventHandler;

public interface CircuitBreaker extends OperationExecutor {

    State getCircuitState();

    <Result, Failure extends Throwable> void submit(Operation<Result, Failure> operation);

    void remove(String key);

    void clear();

    boolean contains(String key);

    <E extends Event> EventBroadcaster.Subscription subscribe(Class<E> eventClass, EventHandler<E> eventHandler);

}
