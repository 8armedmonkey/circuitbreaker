package sys.circuitbreaker;

import sys.event.Event;
import sys.event.EventBroadcaster;
import sys.event.EventHandler;

import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

class TimeoutTimerImpl implements TimeoutTimer {

    private final EventBroadcaster eventBroadcaster;
    private ScheduledExecutorService executorService;
    private Future timeout;

    TimeoutTimerImpl(EventBroadcaster eventBroadcaster) {
        this.eventBroadcaster = eventBroadcaster;
        this.executorService = Executors.newSingleThreadScheduledExecutor();
    }

    @Override
    public synchronized void start(long durationMillis) {
        stop();
        timeout = executorService.schedule(this::timeout, durationMillis, MILLISECONDS);
    }

    @Override
    public synchronized void stop() {
        if (timeout != null) {
            timeout.cancel(false);
            timeout = null;
        }
    }

    @Override
    public synchronized boolean isRunning() {
        return timeout != null && !timeout.isCancelled() && !timeout.isDone();
    }

    @Override
    public <E extends Event> EventBroadcaster.Subscription subscribe(Class<E> eventClass, EventHandler<E> eventHandler) {
        return eventBroadcaster.subscribe(eventClass, eventHandler);
    }

    private void timeout() {
        eventBroadcaster.broadcast(new TimerTimeout());
        timeout = null;
    }

}
