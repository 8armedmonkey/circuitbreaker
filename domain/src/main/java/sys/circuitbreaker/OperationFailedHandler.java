package sys.circuitbreaker;

import sys.event.EventHandler;

@SuppressWarnings("WeakerAccess")
public interface OperationFailedHandler extends EventHandler<OperationFailed> {
}
