package sys.circuitbreaker;

import sys.event.Event;

public class OperationFailed implements Event {

    private final String key;
    private final Throwable throwable;

    public OperationFailed(String key, Throwable throwable) {
        this.key = key;
        this.throwable = throwable;
    }

    public String getKey() {
        return key;
    }

    public Throwable getThrowable() {
        return throwable;
    }

}
