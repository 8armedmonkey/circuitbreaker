package sys.circuitbreaker;

import sys.event.Event;
import sys.event.EventBroadcaster;
import sys.event.EventHandler;

public interface Circuit {

    State getState();

    void close();

    void open();

    void halfOpen();

    void onOperationSuccess();

    void onOperationFailed();

    <E extends Event> EventBroadcaster.Subscription subscribe(Class<E> eventClass, EventHandler<E> eventHandler);

}
