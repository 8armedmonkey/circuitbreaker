package sys.circuitbreaker;

class HalfOpenStateBehavior implements StateBehavior {

    private final Circuit circuit;
    private final Counter successCounter;

    HalfOpenStateBehavior(Circuit circuit, Counter successCounter) {
        this.circuit = circuit;
        this.successCounter = successCounter;
    }

    @Override
    public void onInit() {
        successCounter.reset();
    }

    @Override
    public void onOperationSuccess() {
        successCounter.increment();

        if (successCounter.hasReachedThreshold()) {
            circuit.close();
        }
    }

    @Override
    public void onOperationFailed() {
        circuit.open();
    }

}
