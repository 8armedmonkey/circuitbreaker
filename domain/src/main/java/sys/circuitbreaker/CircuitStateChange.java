package sys.circuitbreaker;

import sys.event.Event;

public class CircuitStateChange implements Event {

    private final State oldState;
    private final State newState;

    public CircuitStateChange(State oldState, State newState) {
        this.oldState = oldState;
        this.newState = newState;
    }

    public State getOldState() {
        return oldState;
    }

    public State getNewState() {
        return newState;
    }

}
