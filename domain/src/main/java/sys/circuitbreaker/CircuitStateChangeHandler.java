package sys.circuitbreaker;

import sys.event.EventHandler;

public interface CircuitStateChangeHandler extends EventHandler<CircuitStateChange> {
}
