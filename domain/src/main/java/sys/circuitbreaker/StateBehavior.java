package sys.circuitbreaker;

public interface StateBehavior {

    void onInit();

    void onOperationSuccess();

    void onOperationFailed();

}
