package sys.circuitbreaker;

@SuppressWarnings("WeakerAccess")
public class CircuitOptions {

    private final State state;
    private final long openStateDurationMillis;

    private CircuitOptions(State state, long openStateDurationMillis) {
        this.state = state;
        this.openStateDurationMillis = openStateDurationMillis;
    }

    public State getState() {
        return state;
    }

    public long getOpenStateDurationMillis() {
        return openStateDurationMillis;
    }

    public static class Builder {

        private State state;
        private long openStateDurationMillis;

        public Builder state(State state) {
            this.state = state;
            return this;
        }

        public Builder openStateDurationMillis(long millis) {
            openStateDurationMillis = millis;
            return this;
        }

        public CircuitOptions build() {
            return new CircuitOptions(state, openStateDurationMillis);
        }

    }

}
