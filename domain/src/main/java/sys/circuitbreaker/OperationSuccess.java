package sys.circuitbreaker;

import sys.event.Event;

public class OperationSuccess<Result> implements Event {

    private final String key;
    private final Result result;

    public OperationSuccess(String key, Result result) {
        this.key = key;
        this.result = result;
    }

    public String getKey() {
        return key;
    }

    public Result getResult() {
        return result;
    }

}
