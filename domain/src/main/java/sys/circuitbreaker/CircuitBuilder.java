package sys.circuitbreaker;

import sys.event.EventBroadcaster;

@SuppressWarnings("WeakerAccess")
public class CircuitBuilder {

    EventBroadcaster eventBroadcaster;
    TimeoutTimer openStateTimer;
    Counter failureCounter;
    Counter successCounter;
    CircuitOptions options;

    public CircuitBuilder eventBroadcaster(EventBroadcaster eventBroadcaster) {
        this.eventBroadcaster = eventBroadcaster;
        return this;
    }

    public CircuitBuilder openStateTimer(TimeoutTimer openStateTimer) {
        this.openStateTimer = openStateTimer;
        return this;
    }

    public CircuitBuilder failureCounter(Counter failureCounter) {
        this.failureCounter = failureCounter;
        return this;
    }

    public CircuitBuilder successCounter(Counter successCounter) {
        this.successCounter = successCounter;
        return this;
    }

    public CircuitBuilder options(CircuitOptions options) {
        this.options = options;
        return this;
    }

    public Circuit build() {
        return new CircuitImpl(
                eventBroadcaster,
                openStateTimer,
                failureCounter,
                successCounter,
                options);
    }

}
