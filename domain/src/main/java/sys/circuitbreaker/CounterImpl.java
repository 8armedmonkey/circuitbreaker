package sys.circuitbreaker;

class CounterImpl implements Counter {

    private final int threshold;
    private int count;

    CounterImpl(int threshold) {
        this(threshold, 0);
    }

    CounterImpl(int threshold, int count) {
        if (threshold < count) {
            throw new IllegalArgumentException();
        }

        this.threshold = threshold;
        this.count = count;
    }

    @Override
    public synchronized void increment() {
        count = Math.min(threshold, count + 1);
    }

    @Override
    public synchronized void reset() {
        count = 0;
    }

    @Override
    public synchronized int current() {
        return count;
    }

    @Override
    public synchronized boolean hasReachedThreshold() {
        return count == threshold;
    }

}
